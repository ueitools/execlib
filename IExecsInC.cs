using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace ExecLib
{
    #region Data Type Definitions
    public enum ReportType : int
    {
        Undefined = 0,     // no report type set yet
        CaptureFile,   // Pure Binary (Like a Capture File)
        FIrpt,         // FI Text report (FrameInfo struct)
        CmprsdFI,      // Compressed output (FI with binary decoded)
        JCode        // Similar to Old style Jcode output
    }

    #endregion

    #region interface IExecsInC

    public interface IExecsInC
    {
        void cInit(string OFPath, int exec, int report);
        void cResetExecInputTable();
        void cResetOutputStream(string OFPath);
        int cRx(string FilePath);
        int cId(string file, int[] execs, int execsSize,
            string genExecWorkPath, string genExecToolsPath);
        void cSetContinuation(int Continuation);
        void cSetData(int Data, int Position);
        int cSetExec(int exec, string GenericCfgPath);
        void cSetKeyLabel(string KeyLabel);
        void cSetOutron(string Outron);
        void cSetPrefix(int Prefix, int Position);
        void cSetReportType(int report);
        void cSetTiming(double Timing);
        void cSetupGenericExec(string GenericCfgPath);
        void cShutdown();
        int cTg();
        void cTx();
    }

    #endregion

    public class ExecsInC : IExecsInC
    {
        #region Imported C++ Functions
        [DllImport("wexec32.dll")]
        static extern void Run_Execsinc(string FilePath, int exec,
            int report);

        [DllImport("wexec32.dll")]
        static extern void Run_Shutdown();

        [DllImport("wexec32.dll")]
        static extern void Run_Tx();

        [DllImport("wexec32.dll")]
        static extern int Run_Rx(string FilePath);

        [DllImport("wexec32.dll")]
        static extern int Run_Id(string file, int[] execs, int execsSize,
            string genExecWorkPath, string genExecToolsPath);

        [DllImport("wexec32.dll")]
        static extern int Run_Tg();

        [DllImport("wexec32.dll")]
        static extern int Run_SetExec(int exec, string cfgfile);

        [DllImport("wexec32.dll")]
        static extern void Run_SetTimingValue(double timing);

        [DllImport("wexec32.dll")]
        static extern void Run_SetReportType(ReportType rpt);

        [DllImport("wexec32.dll")]
        static extern void Run_SetupGenericExec(string genericCfgPath);

        [DllImport("wexec32.dll")]
        static extern void Run_SetKeyLabel(string KeyLabel);

        [DllImport("wexec32.dll")]
        static extern void Run_SetOutron(string Outron);

        [DllImport("wexec32.dll")]
        static extern void Run_SetPrefix(int Prefix, int Position);

        [DllImport("wexec32.dll")]
        static extern void Run_SetData(int Data, int Position);

        [DllImport("wexec32.dll")]
        static extern void Run_SetContinuation(int Continuation);

        [DllImport("wexec32.dll")]
        static extern void Run_ResetExecInputTable();

        [DllImport("wexec32.dll")]
        static extern void Run_ResetOutputStream(string OFPath);
        #endregion

        // upper limit on number of execs
        public const int NUM_EXECS = 997;

        // used for initialization
        public const int INVALID_EXEC = 999;

        // special case generic exec
        public const int GENERIC_EXEC_NUM = 998;

        #region Functions
        public ExecsInC()
        {
        }

        void IExecsInC.cInit(string OFPath, int exec, int report)
        {
            Run_Execsinc(OFPath, exec, report);
        }

        void IExecsInC.cResetExecInputTable() 
        {
            Run_ResetExecInputTable();
        }

        void IExecsInC.cResetOutputStream(string OFPath) 
        {
            Run_ResetOutputStream(OFPath);
        }
        
        int IExecsInC.cRx(string FilePath) 
        {
            return Run_Rx(FilePath);
        }

        void IExecsInC.cSetContinuation(int Continuation) 
        {
            Run_SetContinuation(Continuation);
        }

        void IExecsInC.cSetData(int Data, int Position) 
        {
            Run_SetData(Data, Position);
        }

        int IExecsInC.cSetExec(int exec, string GenericCfgPath) 
        {
            return Run_SetExec(exec, GenericCfgPath);
        }
        
        void IExecsInC.cSetKeyLabel(string KeyLabel) 
        {
            Run_SetKeyLabel(KeyLabel);
        }

        void IExecsInC.cSetOutron(string Outron) 
        {
            Run_SetOutron(Outron);
        }
        
        void IExecsInC.cSetPrefix(int Prefix, int Position) 
        {
            Run_SetPrefix(Prefix, Position);
        }

        void IExecsInC.cSetReportType(int report) 
        {
            Run_SetReportType((ReportType)report);
        }

        void IExecsInC.cSetTiming(double Timing) 
        {
            Run_SetTimingValue(Timing);
        }
        
        void IExecsInC.cSetupGenericExec(string GenericCfgPath) 
        {
            Run_SetupGenericExec(GenericCfgPath);
        }

        void IExecsInC.cShutdown() 
        {
            Run_Shutdown();
        }

        int IExecsInC.cTg() 
        {
            return Run_Tg(); 
        }
        
        void IExecsInC.cTx() 
        {
            Run_Tx();
        }

        int IExecsInC.cId(string file, int[] execs, int execsSize,
            string genExecWorkPath, string genExecToolsPath)
        {
            return Run_Id(file, execs, execsSize, 
                genExecWorkPath, genExecToolsPath);
        }

        #endregion
    }
}
